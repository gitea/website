---
date: "2016-11-08T16:00:00+02:00"
title: "Добро пожаловать"
weight: 10
toc: false
draft: false
url: "ru-ru"
type: "home"
---

<h1 class="title is-1">Gitea - Git with a cup of tea</h1>
<h3 class="subtitle is-3">Удобный сервис собственного хостинга репозиториев Git.</h3>
<h4 class="subtitle">

Gitea - это управляемое сообществом облегчённое решение для хостинга кода, написанное на [Go](https://golang.org/). 
Он публикуется под лицензией [MIT](https://github.com/go-gitea/gitea/blob/master/LICENSE).

</h4>

<div class="container">
<a class="button is-success is-large" href="https://try.gitea.io" target="_blank">Попробовать Gitea</a>
<a class="button is-light is-large" href="https://docs.gitea.io">Документация</a>
</div>
