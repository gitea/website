---
date: "2016-11-08T16:00:00+02:00"
title: "ようこそ"
weight: 10
toc: false
draft: false
url: "ja-jp"
type: "home"
---

<h1 class="title is-1">Gitea - 一杯のお茶を添えた Git</h1>
<h3 class="subtitle is-3">苦痛のないセルフホスト型 Git サービス</h3>
<h4 class="subtitle">

Gitea は、コミュニティ主導で開発が進められている [Go 言語](https://golang.org/)製の軽量のコードホスティングソリューションです。コードは [MIT](https://github.com/go-gitea/gitea/blob/master/LICENSE) ライセンスで公開されています。

</h4>

<div class="container">
<a class="button is-success is-large" href="https://try.gitea.io" target="_blank">Gitea を試す</a>
<a class="button is-light is-large" href="https://docs.gitea.io">ドキュメント</a>
</div>
